#include "Headers/UI/MainWindow.h"
#include "Headers/UI/SetupSerialDialog.h"
#include "ui_MainWindow.h"
#include <QSerialPortInfo>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::mainWindow)
{
    ui->setupUi(this);

    // Setup available serial ports
    QMenu* serialPort = ui->m_menubar->findChild<QMenu*>("m_menuSerialPort");
    if(serialPort != nullptr) {
        QList<QSerialPortInfo> availablePorts = QSerialPortInfo::availablePorts();
        for(const auto& portInfo : availablePorts) {
            QAction* action = serialPort->addAction(portInfo.portName());
            QObject::connect(action, &QAction::triggered, this, [=](){ setupSerial(action->text()); });
        }
    }
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::setupSerial(const QString& portName) {
   SetupSerialDialog setupDialog;
   QSerialPortInfo portInfo(portName);
   setupDialog.SetInfo(portInfo);
   if(setupDialog.exec() == QDialog::Accepted) {
       if(setupDialog.ResultAvailable()) {
           SerialConnectionSetup setup = setupDialog.GetResult();
           // TODO: Establish connection
       }
   }
}
