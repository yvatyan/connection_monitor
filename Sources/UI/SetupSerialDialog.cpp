#include "Headers/UI/SetupSerialDialog.h"
#include "ui_SetupSerialDialog.h"

SetupSerialDialog::SetupSerialDialog(QWidget* parent)
    : QDialog(parent)
    , m_ui(new Ui::setupSerialDialog)
    , m_resultAvailable(false)
{
    m_ui->setupUi(this);
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    setWindowTitle("Serial Connection Setup");
    setFixedSize(geometry().size());

    QObject::connect(m_ui->m_buttonConnect, SIGNAL(clicked()), this, SLOT(createResult()));
    QObject::connect(m_ui->m_buttonCancel, SIGNAL(clicked()), this, SLOT(reject()));
}

void SetupSerialDialog::createResult() {
    m_setup = SerialConnectionSetup(m_ui->m_labelPortValue->text(), m_ui->m_cbBaud->currentText().toInt());
    m_resultAvailable = true;
    accept();
}

void SetupSerialDialog::SetInfo(const QSerialPortInfo& info) {
    assert(!info.portName().isEmpty());
    m_ui->m_labelPortValue->setText(info.portName());
    if(!info.manufacturer().isEmpty()) {
        m_ui->m_labelManufacturerValue->setText(info.manufacturer());
    }
    if(!info.serialNumber().isEmpty()) {
        m_ui->m_labelSerialNumberValue->setText(info.serialNumber());
    }
    if(!info.systemLocation().isEmpty()) {
        m_ui->m_labelSysLocValue->setText(info.systemLocation());
    }
    if(info.hasVendorIdentifier()) {
        m_ui->m_labelVendorIDValue->setText(QString::number(info.vendorIdentifier()));
    }
    if(info.hasProductIdentifier()) {
        m_ui->m_labelProductIDValue->setText(QString::number(info.productIdentifier()));
    }
    if(!info.description().isEmpty()) {
        m_ui->m_labelDescriptionValue->setText(info.description());
    }

    for(const auto& baud : QSerialPortInfo::standardBaudRates()) {
        m_ui->m_cbBaud->addItem(QString::number(baud));
    }
}

