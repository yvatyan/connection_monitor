#include "Headers/SerialConnectionSetup.h"

SerialConnectionSetup::SerialConnectionSetup()
    : m_isValid(false)
{
}
SerialConnectionSetup::SerialConnectionSetup(const QString& portName, int baud)
    : m_isValid(true)
    , m_portName(portName)
    , m_baud(baud)
{
}
