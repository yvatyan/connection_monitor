#ifndef SETUPSERIALDIALOG_H
#define SETUPSERIALDIALOG_H

#include <QDialog>
#include <QSerialPortInfo>
#include "Headers/SerialConnectionSetup.h"

QT_BEGIN_NAMESPACE
namespace Ui {
    class setupSerialDialog;
}
QT_END_NAMESPACE

class SetupSerialDialog : public QDialog {
    Q_OBJECT
public:
    SetupSerialDialog(QWidget* parent = nullptr);

    void SetInfo(const QSerialPortInfo& info);
    inline bool ResultAvailable() const;
    inline SerialConnectionSetup GetResult() const;
private:
    Ui::setupSerialDialog* m_ui;
    bool m_resultAvailable;
    SerialConnectionSetup m_setup;
private slots:
    void createResult();
};

bool SetupSerialDialog::ResultAvailable() const {
    return m_resultAvailable;
}

SerialConnectionSetup SetupSerialDialog::GetResult() const {
    return m_setup;
}

#endif // SETUPSERIALDIALOG_H
