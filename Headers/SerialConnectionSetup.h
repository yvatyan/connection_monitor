#ifndef SERIALCONNECTIONSETUP_H
#define SERIALCONNECTIONSETUP_H

#include <QString>

class SerialConnectionSetup {
public:
    SerialConnectionSetup();
    SerialConnectionSetup(const QString& portName, int baud);

    inline const QString& PortName() const;
    inline int Baud() const;
private:
    bool m_isValid;
    QString m_portName;
    int m_baud;
};

const QString& SerialConnectionSetup::PortName() const {
    return m_portName;
}

int SerialConnectionSetup::Baud() const {
    return m_baud;
}

#endif // SERIALCONNECTIONSETUP_H
